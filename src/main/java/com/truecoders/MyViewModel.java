package com.truecoders;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.MouseEvent;
import org.zkoss.zul.Messagebox;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class MyViewModel {

    private Tasks taskList = new Tasks();
    private Boolean isArchived = Boolean.FALSE;

    public Tasks getTaskList() {
        return taskList;
    }

    public void setTaskList(Tasks taskList) {
        this.taskList = taskList;
    }

    public Boolean getArchived() {
        return isArchived;
    }

    public void setArchived(Boolean archived) {
        isArchived = archived;
    }

    public List<Task> getNonArchivedTasks() {
        return taskList.getTasks()
                .stream()
                .filter(task -> task.getArchived().equals(false))
                .collect(Collectors.toList());
    }

    public List<Task> getArchivedTasks() {
        return taskList.getTasks()
                .stream()
                .filter(task -> task.getArchived().equals(true))
                .collect(Collectors.toList());
    }

    @Command
    public void setDone(@BindingParam("task") Task task, @BindingParam("isDone") Boolean isDone) throws IOException {
        getTaskList().getTasks().get(getTaskList().getTasks().indexOf(task)).setDone(isDone);
        Tasks.saveTasksToJSON(getTaskList());
    }

    @Command
    @NotifyChange("archivedTasks, nonArchivedTasks")
    public void setArchived(@BindingParam("task") Task task, @BindingParam("isArchived") Boolean isArchived) throws IOException {
        Task tempTask = getTaskList().getTasks().get(getTaskList().getTasks().indexOf(task));
        if (tempTask.getDone() || !isArchived) {
            tempTask.setArchived(isArchived);
            Tasks.saveTasksToJSON(getTaskList());
            Executions.sendRedirect(null);
        } else {
            Messagebox.show(
                    "Нельзя отправлять в архив\nневыполненные задачи",
                    "ВНИМАНИЕ",
                    Messagebox.OK,
                    Messagebox.INFORMATION);
        }
    }

    @Command
    public void toggleArchive() {
        setArchived(!getArchived());
        if (getArchived()) {
            getArchivedTasks();
        } else getNonArchivedTasks();
    }

    @Init
    public void init() throws IOException {
        taskList = Tasks.readTasksFromJSON();
    }

    @Command
    public void addTask(@BindingParam("title") String title, @BindingParam("description") String description) throws IOException {
        Task task = new Task();
        task.setId(getTaskList().getTasks().size() + 1);
        task.setTitle(title);
        task.setDescription(description);
        task.setTitle(title);
        task.setDone(false);
        task.setEditable(false);
        task.setArchived(false);

        getTaskList().getTasks().add(task);

        Tasks.saveTasksToJSON(getTaskList());
        Executions.sendRedirect(null);
    }
}
